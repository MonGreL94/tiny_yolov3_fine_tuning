import os
import xml.etree.ElementTree as ET

data_set_dir = 'train/'

class_id = {'one': 0, 'two': 1, 'three': 2, 'four': 3, 'five': 4, 'six': 5, 'arrow_left': 6, 'arrow_right': 7,
            'arrow_up': 8, 'denied': 9, '0_degree': 10, '45_degree': 11, '90_degree': 12, '180_degree': 13,
            'checkpoint_foot': 14, 'wall': 15}

class_shapes_id = {'circle': 0, 'rectangle': 1, 'checkpoint_foot': 2, 'wall': 3}

file_names = os.listdir(data_set_dir)

xml_file_names = []
for file_name in file_names:
    if '.xml' in file_name:
        xml_file_names.append(file_name)

train_text = ''

for xml_file_name in xml_file_names:
    root = ET.parse(data_set_dir + xml_file_name).getroot()
    image_objects = root.findall('object')
    train_text += data_set_dir + xml_file_name.replace('.xml', '.jpg') + ' '

    for obj in image_objects:
        if obj.find('name').text != 'checkpoint_side' and obj.find('name').text != 'checkpoint_signal':

            if obj.find('name').text == 'one' or obj.find('name').text == 'two' or obj.find('name').text == 'three'\
                    or obj.find('name').text == 'four' or obj.find('name').text == 'five'\
                    or obj.find('name').text == 'six' or obj.find('name').text == 'arrow_left'\
                    or obj.find('name').text == 'arrow_right' or obj.find('name').text == 'arrow_up'\
                    or obj.find('name').text == 'denied':

                train_text += obj.find('bndbox/xmin').text + ',' + obj.find('bndbox/ymin').text + ',' + \
                              obj.find('bndbox/xmax').text + ',' + obj.find('bndbox/ymax').text + ',' + \
                              str(class_shapes_id['circle']) + ' '

            elif obj.find('name').text == '0_degree' or obj.find('name').text == '45_degree'\
                    or obj.find('name').text == '90_degree' or obj.find('name').text == '180_degree':

                train_text += obj.find('bndbox/xmin').text + ',' + obj.find('bndbox/ymin').text + ',' + \
                              obj.find('bndbox/xmax').text + ',' + obj.find('bndbox/ymax').text + ',' + \
                              str(class_shapes_id['rectangle']) + ' '

            else:
                train_text += obj.find('bndbox/xmin').text + ',' + obj.find('bndbox/ymin').text + ',' +\
                              obj.find('bndbox/xmax').text + ',' + obj.find('bndbox/ymax').text + ',' +\
                              str(class_shapes_id[obj.find('name').text]) + ' '

    train_text += '\n'

train_text = train_text.replace(' \n', '\n')

with open('train_shapes.txt', 'w') as train_file:
    train_file.write(train_text)
