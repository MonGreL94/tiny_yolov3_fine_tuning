"""
Retrain the YOLO model for your own dataset.
"""

import numpy as np
import keras.backend as K
from keras.layers import Input, Lambda
from keras.models import Model
from keras.optimizers import Adam
from keras.callbacks import TensorBoard, ModelCheckpoint, ReduceLROnPlateau, EarlyStopping
from matplotlib import pyplot as plt
import sys

from yolo3.model import preprocess_true_boxes, tiny_yolo_body, yolo_loss
from yolo3.utils import get_random_data


def _main(log_directory):
    annotation_path = 'train_shapes.txt'
    log_dir = log_directory
    classes_path = 'model_data/custom_classes_shapes.txt'
    anchors_path = 'model_data/custom_anchors_shapes.txt'
    class_names = get_classes(classes_path)
    num_classes = len(class_names)
    anchors = get_anchors(anchors_path)

    input_shape = (480, 640)

    model = create_tiny_model(input_shape, anchors, num_classes, freeze_body=2,
                              weights_path='model_data/ep354_loss17.664_val_loss18.035_weights.h5')

    logging = TensorBoard(log_dir=log_dir)
    checkpoint = ModelCheckpoint(log_dir + 'ep{epoch:03d}_loss{loss:.3f}_val_loss{val_loss:.3f}_weights.h5',
                                 monitor='val_loss', save_weights_only=True, save_best_only=True, period=3)
    reduce_lr = ReduceLROnPlateau(monitor='val_loss', factor=0.1, patience=5, verbose=1)
    early_stopping = EarlyStopping(monitor='val_loss', min_delta=0, patience=100, verbose=1, restore_best_weights=True)

    val_split = 0.3
    with open(annotation_path) as f:
        lines = f.readlines()
    np.random.seed(10101)
    np.random.shuffle(lines)
    np.random.seed(None)
    num_val = int(len(lines) * val_split)
    num_train = len(lines) - num_val

    # Train with frozen layers first, to get a stable loss.
    # Adjust num epochs to your data-set. This step is enough to obtain a not bad model.
    # use custom yolo_loss Lambda layer.
    """model.compile(optimizer=Adam(lr=1e-2), loss={'yolo_loss': lambda y_true, y_pred: y_pred})

    batch_size = 32

    print('Train on {} samples, val on {} samples, with batch size {}.'.format(num_train, num_val, batch_size))

    history = model.fit_generator(data_generator_wrapper(lines[:num_train], batch_size, input_shape, anchors, num_classes),
                                  steps_per_epoch=max(1, num_train // batch_size),
                                  validation_data=data_generator_wrapper(lines[num_train:], batch_size, input_shape, anchors, num_classes),
                                  validation_steps=max(1, num_val // batch_size),
                                  epochs=100,
                                  initial_epoch=0,
                                  callbacks=[logging, checkpoint])

    model.save_weights(log_dir + 'trained_model_weights_stage_1.h5')

    t = [k + 1 for k in range(len(history.history['loss']))]
    plt.plot(t, history.history['loss'])
    plt.plot(t, history.history['val_loss'])
    plt.title('model loss')
    plt.ylabel('loss')
    plt.xlabel('epoch')
    plt.legend(['train', 'val'], loc='upper left')
    plt.savefig(log_dir + 'output_loss_stage_1.png')
    plt.gcf().clear()

    # Unfreeze all except darknet body and continue training, to fine-tune.
    for i in range(20, len(model.layers)):
        model.layers[i].trainable = True
    model.compile(optimizer=Adam(lr=1e-3), loss={'yolo_loss': lambda y_true, y_pred: y_pred})  # recompile to apply the change
    print('Unfreeze all of the layers except darknet body.')

    batch_size = 32

    print('Train on {} samples, val on {} samples, with batch size {}.'.format(num_train, num_val, batch_size))

    history = model.fit_generator(data_generator_wrapper(lines[:num_train], batch_size, input_shape, anchors, num_classes),
                                  steps_per_epoch=max(1, num_train // batch_size),
                                  validation_data=data_generator_wrapper(lines[num_train:], batch_size, input_shape, anchors, num_classes),
                                  validation_steps=max(1, num_val // batch_size),
                                  epochs=200,
                                  initial_epoch=100,
                                  callbacks=[logging, checkpoint])

    model.save_weights(log_dir + 'trained_model_weights_stage_2.h5')

    t = [k + 1 for k in range(len(history.history['loss']))]
    plt.plot(t, history.history['loss'])
    plt.plot(t, history.history['val_loss'])
    plt.title('model loss')
    plt.ylabel('loss')
    plt.xlabel('epoch')
    plt.legend(['train', 'val'], loc='upper left')
    plt.savefig(log_dir + 'output_loss_stage_2.png')
    plt.gcf().clear()"""

    # Unfreeze all layers and continue training, to fine-tune.
    # Train longer if the result is not good.
    for i in range(len(model.layers)):
        model.layers[i].trainable = True
    model.compile(optimizer=Adam(lr=1e-2), loss={'yolo_loss': lambda y_true, y_pred: y_pred})  # recompile to apply the change
    print('Unfreeze all of the layers.')

    batch_size = 32

    print('Train on {} samples, val on {} samples, with batch size {}.'.format(num_train, num_val, batch_size))

    history = model.fit_generator(data_generator_wrapper(lines[:num_train], batch_size, input_shape, anchors, num_classes),
                                  steps_per_epoch=max(1, num_train//batch_size),
                                  validation_data=data_generator_wrapper(lines[num_train:], batch_size, input_shape, anchors, num_classes),
                                  validation_steps=max(1, num_val//batch_size),
                                  epochs=400,
                                  initial_epoch=300,
                                  callbacks=[logging, checkpoint, reduce_lr, early_stopping])

    model.save_weights(log_dir + 'trained_model_weights_final.h5')

    t = [k + 1 for k in range(len(history.history['loss']))]
    plt.plot(t, history.history['loss'])
    plt.plot(t, history.history['val_loss'])
    plt.title('model loss')
    plt.ylabel('loss')
    plt.xlabel('epoch')
    plt.legend(['train', 'val'], loc='upper left')
    plt.savefig(log_dir + 'output_loss_final.png')
    plt.gcf().clear()

    t = [k + 1 for k in range(len(history.history['lr']))]
    plt.plot(t, history.history['lr'])
    plt.title('model lr')
    plt.ylabel('lr')
    plt.xlabel('epoch')
    plt.legend(['lr'], loc='upper left')
    plt.savefig(log_dir + 'learning_rate.png')
    plt.gcf().clear()

    # Further training if needed.


def get_classes(classes_path):
    with open(classes_path) as f:
        class_names = f.readlines()
    class_names = [c.strip() for c in class_names]
    return class_names


def get_anchors(anchors_path):
    with open(anchors_path) as f:
        anchors = f.readline()
    anchors = [float(x) for x in anchors.split(',')]
    return np.array(anchors).reshape(-1, 2)


def create_tiny_model(input_shape, anchors, num_classes, load_pretrained=True, freeze_body=2, weights_path='model_data/tiny_yolo_weights_coco.h5'):
    K.clear_session()
    image_input = Input(shape=(None, None, 3))
    h, w = input_shape
    num_anchors = len(anchors)

    y_true = [Input(shape=(h//{0: 32, 1: 16}[l], w//{0: 32, 1: 16}[l], num_anchors//2, num_classes+5)) for l in range(2)]

    model_body = tiny_yolo_body(image_input, num_anchors//2, num_classes)
    print('Create Tiny YOLOv3 model with {} anchors and {} classes.'.format(num_anchors, num_classes))

    if load_pretrained:
        model_body.load_weights(weights_path, by_name=True, skip_mismatch=True)
        print('Load weights {}.'.format(weights_path))
        if freeze_body in [1, 2]:
            # Freeze the darknet body or freeze all but 2 output layers.
            num = (20, len(model_body.layers)-2)[freeze_body-1]
            for i in range(num):
                model_body.layers[i].trainable = False
            print('Freeze the first {} layers of total {} layers.'.format(num, len(model_body.layers)))

    model_loss = Lambda(yolo_loss, output_shape=(1,), name='yolo_loss', arguments={'anchors': anchors,
                                                                                   'num_classes': num_classes,
                                                                                   'ignore_thresh': 0.5})([*model_body.output, *y_true])

    model = Model([model_body.input, *y_true], model_loss)

    return model


def data_generator(annotation_lines, batch_size, input_shape, anchors, num_classes):
    n = len(annotation_lines)
    i = 0
    while True:
        image_data = []
        box_data = []
        for b in range(batch_size):
            if i == 0:
                np.random.shuffle(annotation_lines)
            image, box = get_random_data(annotation_lines[i], input_shape, random=True)
            image_data.append(image)
            box_data.append(box)
            i = (i+1) % n
        image_data = np.array(image_data)
        box_data = np.array(box_data)
        y_true = preprocess_true_boxes(box_data, input_shape, anchors, num_classes)
        yield [image_data, *y_true], np.zeros(batch_size)


def data_generator_wrapper(annotation_lines, batch_size, input_shape, anchors, num_classes):
    n = len(annotation_lines)
    if n == 0 or batch_size <= 0:
        return None
    return data_generator(annotation_lines, batch_size, input_shape, anchors, num_classes)


if __name__ == '__main__':
    _main(sys.argv[1])
